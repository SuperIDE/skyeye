/* Copyright (C)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*/
/**
* @file timer_1ms_am3359.h
* @brief The definition of system controller
* @author zyumig: zyumingfit@gmail.com
* @version 78.77
*/

/* Autogenerated by SkyEye script */
#ifndef __timer_1ms_am3359_H__
#define __timer_1ms_am3359_H__
#include <stdint.h>
#include <skyeye_lock.h>

#define INTERVAL 100
typedef struct timer_1ms_am3359_device{
	conf_object_t* obj;
	struct registers {
		uint32_t tidr;
		uint32_t tiocp_cfg;
		uint32_t tistat;
		uint32_t tisr;
		uint32_t tier;
		uint32_t twer;
		uint32_t tclr;
		uint32_t tcrr;
		uint32_t tldr;
		uint32_t ttgr;
		uint32_t twps;
		uint32_t tmar;
		uint32_t tcar1;
		uint32_t tsicr;
		uint32_t tcar2;
		uint32_t tpir;
		uint32_t tnir;
		uint32_t tcvr;
		uint32_t tocr;
		uint32_t towr;
	}regs;

	int scheduler_id;
	uint32_t irqenable;
	general_signal_intf *signal_iface;
	conf_object_t *signal;
	uint32_t interrupt_number;
	RWLOCK_T lock;
}timer_1ms_am3359_device;

#define DMTIMER_1MS_TIDR		(0x0)
#define DMTIMER_1MS_TIOCP_CFG		(0x10)
#define DMTIMER_1MS_TISTAT		(0x14)
#define DMTIMER_1MS_TISR		(0x18)
#define DMTIMER_1MS_TIER		(0x1c)
#define DMTIMER_1MS_TWER		(0x20)
#define DMTIMER_1MS_TCLR		(0x24)
#define DMTIMER_1MS_TCRR		(0x28)
#define DMTIMER_1MS_TLDR		(0x2c)
#define DMTIMER_1MS_TTGR		(0x30)
#define DMTIMER_1MS_TWPS		(0x34)
#define DMTIMER_1MS_TMAR		(0x38)
#define DMTIMER_1MS_TCAR1		(0x3c)
#define DMTIMER_1MS_TSICR		(0x40)
#define DMTIMER_1MS_TCAR2		(0x44)
#define DMTIMER_1MS_TPIR		(0x48)
#define DMTIMER_1MS_TNIR		(0x4c)
#define DMTIMER_1MS_TCVR		(0x50)
#define DMTIMER_1MS_TOCR		(0x54)
#define DMTIMER_1MS_TOWR		(0x58)

static char *regs_name[] = {
		"tidr",
		"tiocp_cfg",
		"tistat",
		"tisr",
		"tier",
		"twer",
		"tclr",
		"tcrr",
		"tldr",
		"ttgr",
		"twps",
		"tmar",
		"tcar1",
		"tsicr",
		"tcar2",
		"tpir",
		"tnir",
		"tcvr",
		"tocr",
		"towr",
		NULL
};
#endif
