/* Copyright (C)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
/**
 * @file fight_control_sparc.h
 * @brief The definition of system controller 
 * @author 
 * @version 78.77
 */

/* Autogenerated by SkyEye script */
#ifndef __FIGHT_CONTROL_SPARC_H__
#define __FIGHT_CONTROL_SPARC_H__
#include "skyeye_system.h"

typedef struct
{
    uint32_t slock_command_X[28];//0x10018800 自锁阀指令1-11 xx为9位数据
    uint32_t slock_command_T[28];//0x10018804 自锁阀指令1-11 0x3 
    uint32_t slock_command_A[28];//0x10018808 自锁阀指令1-11 0xA5A5 
    uint32_t attitude_command_X[8];//0x10018950 姿控指令1-12 xx为9位数据
    uint32_t attitude_command_T[8];//0x10018954 姿控指令1-12 0x3 
    uint32_t attitude_command_A[8];//0x10018958 姿控指令1-12 0xA5A5 
    uint32_t orbit_command_X[12]; //0x100189B0 轨控编队指令1-12 XX低16位数据 
    uint32_t orbit_command_F[12]; //0x100189B4 轨控编队指令1-12 0xF高4位数据 
    uint32_t orbit_command_T[12]; //0x100189B8 轨控编队指令1-12  0x3
    uint32_t orbit_command_A[12]; //0x100189BC 轨控编队指令1-12  0xA5A5
    uint32_t power_command_X[48];//0x1001c800 飞轮通电指令1-11 xx为9位数据
    uint32_t power_command_T[48];//0x1001c804 飞轮通电指令1-11 0x3 
    uint32_t power_command_A[48];//0x1001c808 飞轮通电指令1-11 0xA5A5 
    uint32_t orbit_command_mode;//轨控指令输出方式寄存器 //AABB 定时输出，5544 直接输出
    uint32_t orbit_timer_reg; //写轨控定时寄存器地址
    uint32_t orbit_mode_reset;//轨控输出模式复位寄存器
    uint32_t unlock_reg; //解锁寄存器 
}fight_control_sparc_reg_t;

uint32_t power_id[48] = {0, 1, 2, 3, 4, 5, -1, -1, 6, 7, -1, -1, 8, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 12, 13, 14, 15, 16, 17, 18, 19, -1, 20, 21, 22, -1, -1};

typedef union
{
    uint32_t u32;
    struct
    {
        uint16_t lsb;
        uint16_t msb;
    }u16;
}orbit_width_t;

typedef struct fight_control_sparc_device
{
    conf_object_t* obj;
    fight_control_sparc_reg_t* regs;
    int timer_scheduler_id;
    conf_object_t* intf_obj;
    skyeye_ad_data_intf* ad_intf;
    pulse_linker_intf* pulse_linker_iface;
    conf_object_t* pulse_linker;
    time_handle_t handle;
    time_handle_t timer_handler;
    int sched_ms;
    int delay_time_cnt;
    orbit_width_t orbit_width[12];
    get_aocs_intf* aocs_iface;
    conf_object_t* aocs;
}fight_control_sparc_device;

#define SLOCK_COMMAND_0    0
#define ATTITUDE_COMMAND_0 0x150
#define ORBIT_COMMAND_0    0x1B0
#define ORBIT_COMMAND_MODE 0x270
#define ORBIT_TIMER_REG    0x274
#define ORBIT_MODE_RESET   0x278
#define UNLOCK_REG         0x27C
#define POWER_COMMAND_0    0x4000
#define POWER_COMMAND_47   0x4260

#define IO_4001 	1
#define IO_4002 	2
#define IO_4003 	3

#define IO_POWER_NUM 	3
#define IO_COMMAND_NUM 	1

static char* regs_name[] =
{
    "slock_command_X_1",
    "slock_command_T_1",
    "slock_command_A_1",
    "slock_command_X_2",
    "slock_command_T_2",
    "slock_command_A_2",
    "slock_command_X_3",
    "slock_command_T_3",
    "slock_command_A_3",
    "slock_command_X_4",
    "slock_command_T_4",
    "slock_command_A_4",
    "slock_command_X_5",
    "slock_command_T_5",
    "slock_command_A_5",
    "slock_command_X_6",
    "slock_command_T_6",
    "slock_command_A_6",
    "slock_command_X_7",
    "slock_command_T_7",
    "slock_command_A_7",
    "slock_command_X_8",
    "slock_command_T_8",
    "slock_command_A_8",
    "slock_command_X_9",
    "slock_command_T_9",
    "slock_command_A_9",
    "slock_command_X_10",
    "slock_command_T_10",
    "slock_command_A_10",
    "slock_command_X_11",
    "slock_command_T_11",
    "slock_command_A_11",
    "attitude_command_X_1",
    "attitude_command_T_1",
    "attitude_command_A_1",
    "attitude_command_X_2",
    "attitude_command_T_2",
    "attitude_command_A_2",
    "attitude_command_X_3",
    "attitude_command_T_3",
    "attitude_command_A_3",
    "attitude_command_X_4",
    "attitude_command_T_4",
    "attitude_command_A_4",
    "attitude_command_X_5",
    "attitude_command_T_5",
    "attitude_command_A_5",
    "attitude_command_X_6",
    "attitude_command_T_6",
    "attitude_command_A_6",
    "attitude_command_X_7",
    "attitude_command_T_7",
    "attitude_command_A_7",
    "attitude_command_X_8",
    "attitude_command_T_8",
    "attitude_command_A_8",
    "orbit_command_X_1",
    "orbit_command_F_1",
    "orbit_command_T_1",
    "orbit_command_A_1",
    "orbit_command_X_2",
    "orbit_command_F_2",
    "orbit_command_T_2",
    "orbit_command_A_2",
    "orbit_command_X_3",
    "orbit_command_F_3",
    "orbit_command_T_3",
    "orbit_command_A_3",
    "orbit_command_X_4",
    "orbit_command_F_4",
    "orbit_command_T_4",
    "orbit_command_A_4",
    "orbit_command_X_5",
    "orbit_command_F_5",
    "orbit_command_T_5",
    "orbit_command_A_5",
    "orbit_command_X_6",
    "orbit_command_F_6",
    "orbit_command_T_6",
    "orbit_command_A_6",
    "orbit_command_X_7",
    "orbit_command_F_7",
    "orbit_command_T_7",
    "orbit_command_A_7",
    "orbit_command_X_8",
    "orbit_command_F_8",
    "orbit_command_T_8",
    "orbit_command_A_8",
    "orbit_command_X_9",
    "orbit_command_F_9",
    "orbit_command_T_9",
    "orbit_command_A_9",
    "orbit_command_X_10",
    "orbit_command_F_10",
    "orbit_command_T_10",
    "orbit_command_A_10",
    "orbit_command_X_11",
    "orbit_command_F_11",
    "orbit_command_T_11",
    "orbit_command_A_11",
    "orbit_command_X_12",
    "orbit_command_F_12",
    "orbit_command_T_12",
    "orbit_command_A_12",
    "orbit_command_mode",
    "orbit_timer_reg",
    "orbit_mode_reset",
    "unlock_reg",
    NULL
};

#define OBT_LENGTH 12
#define REG_NUM 109
#endif
