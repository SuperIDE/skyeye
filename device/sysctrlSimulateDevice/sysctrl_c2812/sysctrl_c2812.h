/* Copyright (C)
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
* 
*/
/**
* @file syscrtl_c2812.h
* @brief The definition of system controller 
* @author 
* @version 78.77
*/

/* Autogenerated by SkyEye script */
#ifndef __SYSCTRL_c2812_H__
#define __SYSCTRL_c2812_H__
#include <skyeye_system.h>

#ifdef __cplusplus
extern "C" {
#endif

//---------------------------------------------------------------------------
// System Control Individual Register Bit Definitions:
//

// High speed peripheral clock register bit definitions:
struct HISPCP_BITS  {   // bits  description
   uint16_t HSPCLK:3;     // 2:0   Rate relative to SYSCLKOUT
   uint16_t rsvd1:13;     // 15:3  reserved
};

union HISPCP_REG {
   uint16_t              all;
   struct HISPCP_BITS  bit;
};

// Low speed peripheral clock register bit definitions:
struct LOSPCP_BITS  {   // bits  description
   uint16_t LSPCLK:3;     // 2:0   Rate relative to SYSCLKOUT
   uint16_t rsvd1:13;     // 15:3  reserved
};

union LOSPCP_REG {
   uint16_t              all;
   struct LOSPCP_BITS  bit;
};

// Peripheral clock control register 1 bit definitions:
struct PCLKCR_BITS  {    // bits  description
   uint16_t EVAENCLK:1;   // 0     Enable SYSCLKOUT to EPWM1
   uint16_t EVBENCLK:1;   // 1     Enable SYSCLKOUT to EPWM2
   uint16_t rsvd0:1;      // 2     reserved
   uint16_t ADCENCLK:1;   // 3     Enable SYSCLKOUT to EPWM3
   uint16_t rsvd1:4;      // 7:4   reserved
   uint16_t SPIENCLK:1;   // 8     Enable SYSCLKOUT to EPWM4
   uint16_t rsvd2:1;      // 9     reserved
   uint16_t SCIAENCLK:1;  // 10    Enable SYSCLKOUT to SCIA
   uint16_t SCIBENCLK:1;  // 11    Enable SYSCLKOUT to SCIB
   uint16_t MCBSPENCLK:1; // 12    Enable SYSCLKOUT to SCIB
   uint16_t rsvd3:1;      // 13    reserved
   uint16_t ECANENCLK:1;  // 14    Enable SYSCLKOUT to EPWM6
   uint16_t rsvd4:1;      // 15    reserved
};

union PCLKCR_REG {
   uint16_t              all;
   struct PCLKCR_BITS bit;
};


// Peripheral clock control register 2 bit definitions:


// PLL control register bit definitions:
struct PLLCR_BITS {      // bits  description
   uint16_t DIV:4;         // 3:0   Set clock ratio for the PLL
   uint16_t rsvd1:12;      // 15:4  reserved
};

union PLLCR_REG {
   uint16_t             all;
   struct PLLCR_BITS  bit;
};

// Low Power Mode 0 control register bit definitions:
struct LPMCR0_BITS {     // bits  description
   uint16_t LPM:2;         // 1:0   Set the low power mode
   uint16_t QUALSTDBY:6;   // 7:2   Qualification
   uint16_t rsvd1:8;       // 15:8  reserved
};

union LPMCR0_REG {
   uint16_t              all;
   struct LPMCR0_BITS  bit;
};

// Low Power Mode 0 control register bit definitions:
//  If the respective bit is set to 1, it enables the selected signal to wake the device from STANDBY mode. If the bit is cleared, the signal has no effect.
struct LPMCR1_BITS {     // bits  description
   uint16_t XINT1:1;       // 0   
   uint16_t XNMI:1;   	   // 1   
   uint16_t WDINT:1;       // 2  
   uint16_t T1CTRIP:1;     // 3  
   uint16_t T2CTRIP:1;     // 4  
   uint16_t T3CTRIP:1;     // 5  
   uint16_t T4CTRIP:1;     // 6  
   uint16_t C1TRIP:1;      // 7  
   uint16_t C2TRIP:1;      // 8  
   uint16_t C3TRIP:1;      // 9  
   uint16_t C4TRIP:1;      // 10  
   uint16_t C5TRIP:1;      // 11  
   uint16_t C6TRIP:1;      // 12  
   uint16_t SCIRXA:1;      // 13  
   uint16_t SCIRXB:1;      // 14  
   uint16_t CANRX:1;       // 15  
};

union LPMCR1_REG {
   uint16_t              all;
   struct LPMCR1_BITS  bit;
};

// Dual-mapping configuration register bit definitions:
struct MAPCNF_BITS {     // bits  description
    uint16_t MAPEPWM:1;    // 0     EPWM dual-map enable
    uint16_t rsvd1:15;     // 15:1  reserved
};

union MAPCNF_REG {
	uint16_t             all;
	struct MAPCNF_BITS bit;
};

struct SCSR_BITS {      // bits  description
   uint16_t WDOVERRIDE:1;       // 0  If WDOVERRIDE is set to 1, the user is allowed to change the state of the Watchdog disable (WDDIS) bit in the Watchdog Control (WDCR) register 
   uint16_t WDENINT:1;      	// 1 Watchdog enable interrupt 
   uint16_t WDINTS:1;      	// 2 Watchdog interrupt status bit
   uint16_t reserved:13;      	// 15:3  reserved
};

union SCSR_REG {
   uint16_t           all;
   struct SCSR_BITS  bit;
};
//---------------------------------------------------------------------------
// System Control Register File:
//
typedef struct SYS_CTRL_REGS {
   union   HISPCP_REG  HISPCP;    // 10: High-speed peripheral clock pre-scaler
   union   LOSPCP_REG  LOSPCP;    // 11: Low-speed peripheral clock pre-scaler
   union   PCLKCR_REG  PCLKCR;   // 13: Peripheral clock control register
   union   LPMCR0_REG  LPMCR0;    // 14: Low-power mode control register 0
   union   LPMCR1_REG  LPMCR1;    // 14: Low-power mode control register 1
   union   PLLCR_REG   PLLCR;     // 17: PLL control register
   // No bit definitions are defined for SCSR because
   // a read-modify-write instruction can clear the WDOVERRIDE bit
   union   SCSR_REG   SCSR;      // 18: System control and status register
   uint16_t              WDCNTR;    // 19: WD counter register
   uint16_t              WDKEY;     // 21: WD reset key register
   // No bit definitions are defined for WDCR because
   // the proper value must be written to the WDCHK field
   // whenever writing to this register.
   uint16_t              WDCR;      // 25: WD timer control register
   union   MAPCNF_REG  MAPCNF;    // 30: Dual-mapping configuration register
}sysctrl_c2812_reg_t;

typedef struct syscrtl_c2812_device{
	conf_object_t* obj;
	sysctrl_c2812_reg_t* regs;
	general_signal_intf* signal_iface;
	conf_object_t* signal; 
    	time_handle_t *handle;
	int old_key;
	int divsel;
	int oscclk;
	int sysclk;
	double wdclk;
	int wd_scale;
	int sched_id;
}sysctrl_c2812_device;

#define WDCHK ((dev->regs->WDCR >> 3) & 0x7)
#define WDDIS ((dev->regs->WDCR >> 6) & 0x1)
static char* regs_name[] = {
	"HISPCP",
	"LOSPCP",
	"PCLKCR",
	"LPMCR0",
	"LPMCR1",
	"PLLCR",
	"SCSR",
	"WDCNTR",
	"WDKEY",
	"WDCR",
	NULL
};
static uint32_t regs_offset[] = {
	0x0009,
	0x000a,
	0x000b,
	0x000d,
	0x000e,
	0x0010,
	0x0011,
	0x0012,
	0x0014,
	0x0018,
};

#define HISPCP_REG 0x0A
#define LOSPCP_REG 0x0B
#define PCLKCR_REG 0x0C
#define LPMCR0_REG 0x0E
#define LPMCR1_REG 0x0F
#define PLLCR_REG  0x11
#define SCSR_REG   0x12
#define WDCNTR_REG 0x13
#define WDKEY_REG  0x15
#define WDCR_REG   0x19

#ifdef __cplusplus
}
#endif 

#endif
