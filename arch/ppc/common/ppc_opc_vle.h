
#ifndef __PPC_OPC_VLE_H__
#define __PPC_OPC_VLE_H__

void ppc_vle_opc_b();
void ppc_vle_se_opc_b();
void ppc_vle_opc_bc();
void ppc_vle_se_opc_bc();
void ppc_vle_se_opc_bclri();
void ppc_vle_se_opc_bctr();
void ppc_vle_se_opc_bgeni();
void ppc_vle_se_opc_blr();
void ppc_vle_se_opc_bmaski();
void ppc_vle_se_opc_bseti();
void ppc_vle_se_opc_btsti();
void ppc_vle_opc_mcrf();
void ppc_vle_se_opc_mfar();
void ppc_vle_se_opc_mfctr();
void ppc_vle_se_opc_mflr();
void ppc_vle_se_opc_mr();
void ppc_vle_se_opc_mtar();
void ppc_vle_se_opc_mtctr();
void ppc_vle_se_opc_mtlr();
void ppc_vle_se_opc_rfci();
void ppc_vle_se_opc_rfdi();
void ppc_vle_se_opc_rfgi();
void ppc_vle_se_opc_rfi();
void ppc_vle_se_opc_rfmci();
void ppc_vle_opc_sc();
void ppc_vle_se_opc_sc();


#endif

