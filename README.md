# 背景介绍

SkyEye 起源于2002年冬天，一群操作系统爱好者在网络上讨论技术，想要在脱离硬件限制的情况下，学习和研究操作系统的原理。于是，他们提出用软件模拟硬件的想法，这其中就包括 SkyEye 的发起者——清华大学陈渝教授。

当时，国际上已经有类似的项目，如 uCLinux 组织开发的 ARMulator 模拟器，可以模拟 AT91 的开发板，支持 UClinux 在上面运行。基于 ARMulator 的设计框架和经验，陈渝教授等人在 2003 年正式发起了 SkyEye 开源项目，被国内外个人爱好者和高校大量使用。

经过二十年的技术沉淀和发展，SkyEye 已完成商业孵化，成为国产 MBSE 工业软件的代表之一，被广泛应用于航空、航天、轨交、机电等领域。

# 项目介绍

Open-SkyEye 是一个开源软件（OpenSource Software）项目，是 SkyEye 的开源（社区）版本。其目标是在通用的 Linux、Windows、Mac OS 等平台上实现一个纯软件集成开发环境，模拟常见的嵌入式计算机系统(这里假定"仿真"和"模拟"的意思基本相同)；

用户可以在 Open-SkyEye 上运行 linux、RTlinux、RTThread、FreeRTOS、μC/OS、LiteOS 等多种嵌入式操作系统和各种系统软件（如TCP/IP，图形子系统，文件子系统等），并可对它们进行源码级的分析和测试。

目前开源（社区）版本由浙江迪捷软件科技有限公司开发和维护，[查看商用版SkyEye介绍](https://www.digiproto.com/product/24.html)。

****

# 快速开始

Open-SkyEye 分为发行版和开发版。发行版配置相对简单，无需源码编译和下载用例，采用虚拟机 + Ubuntu 安装，适合初学者使用；开发版可以基于最新 Open-SkyEye 源码进行编译使用，实时体验到新功能和用例，采用 WSL + Ubuntu 安装，适合有开发经验者使用。

### （一）发行版镜像快速安装

以 ubuntu16.04 为例，首先启动 Docker 服务，命令为`sudo service docker start`，**不同的操作系统或者平台，可能方法不同，请自行查阅**。

推荐使用命令拉取镜像：

`sudo docker pull skyeyehub/opt:v1.3.6-beta`

[或者手动下载镜像(需要账户登录)](https://hub.docker.com/repository/docker/skyeyehub/opt)。

镜像下载好以后，可以使用`sudo docker images`命令，查看镜像列表，看看是否导入成功：

![](docs/readme/media/docker-images1.gif)

为了保证正常的画面显示，宿主机还需要安装一些软件，请使用如下命令（不同操作系统，安装命令可能不同）：

```
sudo apt-get install x11-xserver-utils && xhost +
```

这个命令的含义是开放权限，允许所有用户访问 X11 的显示接口。

现在，我们基于这个镜像，新建一个容器，这个命令有些复杂，先贴出来再解释：

```
sudo docker run -it \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-e DISPLAY=:0 \
-e GDK_SCALE \
-e GDK_DPI_SCALE \
--name open-skyeye skyeyehub/opt:v1.3.6-beta /bin/bash
```

首先，`run`是创建容器的基本命令，`-v  /tmp/.X11-unix:/tmp/.X11-unix`、`-e DISPLAY=:0 -e GDK_SCALE -e GDK_DPI_SCALE`是将宿主机的显示能力共享给容器，请使用如下命令获取图形显示的位置：

```
printenv DISPLAY
```

假如获取的值是`:0.0`，那么在创建容器的时候，就需要更新`-e DISPLAY=:0`这部分。

 `--name open-skyeye skyeyehub/dev:v1`是给容器命名，后面跟着的是镜像名称，`/bin/bash`是以bash启动。更多关于 Docker 的使用方法和命令，后续由其他章节来描述。

但是共享主机的显示能力不是必须的，如果你不使用 Open-SkyEye 的 UI 和串口交互软件 putty。单纯创建一个没有显示能力的容器命令如下：

```
sudo docker run -it \
-p 65500:65500 \
--name open-skyeye skyeyehub/opt:v1.3.6-beta /bin/bash
```
其中`-p 65500:65500`是共享的ip端口，用来通过telnet访问目标机的串口数据。

注意如果要创建多个容器，需要修改 --name 后面的名称，**容器不允许重名**。

创建好容器，自动进入 bash，效果如下：

![](docs/readme/media/create-container1.gif)

现在可以输出命令 skyeye，启动 Open-SkyEye了：

![](docs/readme/media/skyeye.gif)

### （二）开发版镜像快速安装

#### 1. 准备工作

Open-SkyEye 的发行版，是基于 Docker 打包的镜像，用户只需要安装 Docker，就能正常使用和开发 Open-SkyEye。目前我们测试过 ubuntu、archlinux、windows、macos等多个操作系统平台，只要支持 Docker 的生产环境，基本都可以运行 Open-SkyEye。

除了安装 Docker 和下载镜像，你还需要 clone 或者下载一些代码（ Open-SkyEye 源代码和测试用例）。链接清单如下：

[Open-SkyEye 源代码](https://gitee.com/open-skyeye/code/tags) (或 git clone https://gitee.com/open-skyeye/code.git)

[Open-SkyEye 测试用例](https://gitee.com/open-skyeye/testcase) (或 git clone https://gitee.com/open-skyeye/testcase.git)

源代码获取，优先选择 tag 标签的版本。

Docker 安装教程比较简单，这里不再作过多赘述，可以根据自己的环境，在网上查阅资料以后安装。

为了方便开发和调试，在下载好源代码和测试用例以后，请建立一个文件夹，将它们包含进去，这个文件夹，将被共享给容器使用。

```
mkdir /home/zevorn/skyeye-workspace
cd /home/zevorn/skyeye-workspace
git clone -b v1.3.6-beta https://gitee.com/open-skyeye/code.git
git clone https://gitee.com/open-skyeye/testcase.git
```
#### 2. 导入镜像，并新建容器

以 ubuntu20.04 为例，首先启动 Docker 服务，命令为`sudo service docker start`，**不同的操作系统或者平台，可能方法不同，请自行查阅**。

[点击我下载镜像(需要登录dockerhub)](https://hub.docker.com/repository/docker/skyeyehub/dev)，或者使用命令下载：

`sudo docker pull skyeyehub/dev:v1.0.0-rc0`

这个时候，可以使用`docker images`命令，查看镜像列表，看看是否导入成功：

![](docs/readme/media/docker-images.gif)

为了保证正常的画面显示，宿主机还需要安装一些软件，请使用如下命令（不同操作系统，安装命令可能不同）：

```
sudo apt-get install x11-xserver-utils && xhost +
```

这个命令的含义是开放权限，允许所有用户访问 X11 的显示接口。

现在，我们基于这个镜像，新建一个容器，这个命令有些复杂，先贴出来再解释：

```
docker run -it \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-v /home/zevorn/skyeye-workspace/:/home/skyeye-workspace \
-e DISPLAY=:0 \
-e GDK_SCALE \
-e GDK_DPI_SCALE \
--name open-skyeye skyeyehub/dev:v1.0.0-rc0 /bin/bash
```

首先，`run`是创建容器的基本命令，`-v  /tmp/.X11-unix:/tmp/.X11-unix`、`-e DISPLAY=:0 -e GDK_SCALE -e GDK_DPI_SCALE`是将宿主机的显示能力共享给容器，请使用如下命令获取图形显示的位置：

```
printenv DISPLAY
```

假如获取的值是`:0.0`，那么在创建容器的时候，就需要更新`-e DISPLAY=:0`这部分。

其次，`-v /home/zevorn/skyeye-workspace/:/home/skyeye-workspace`是将一个文件夹共享给容器，Open-SkyEye 的源代码和测试用例都放在这个文件夹下。 `--name open-skyeye skyeyehub/dev:v1`是给容器命名，后面跟着的是镜像名称，`/bin/bash`是以bash启动。更多关于 Docker 的使用方法和命令，后续由其他章节来描述。

但是共享主机的显示能力不是必须的，如果你不使用 Open-SkyEye 的 UI 和串口交互软件 putty。单纯创建一个没有显示能力的容器命令如下：

```
docker run -it \
-v /home/zevorn/skyeye-workspace/:/home/skyeye-workspace \
-p 65500:65500 \
--name open-skyeye skyeyehub/dev:v1.0.0-rc0 /bin/bash
```
其中`-p 65500:65500`是共享的 ip 端口，用来通过 telnet 访问目标机的串口数据。

注意如果要创建多个容器，需要修改 --name 后面的名称，容器不允许重名。

#### 3. 编译Open-SkyEye

创建好容器，自动进入 bash，效果如下：

![](docs/readme/media/create-container.gif)

接着我们进入共享给容器的目录，输入命令`cd /home/skyeye-workspace/code && ls`，顺便浏览一下代码目录：

![](docs/readme/media/03.png)

现在，确定无误后，我们现在可以编译 Open-SkyEye 了！请输入以下命令：

```
./autogen.sh && ./configure --prefix=/home/skyeye-workspace/opt/skyeye && make -j4 && make install
```

![](docs/readme/media/build-skyeye.gif)

如果不指定./configure --prefix，那么 Open-SkyEye 将默认安装在`/opt/skyeye`下面。

等待编译完后，看到如下图：

![](docs/readme/media/04.png)

现在可以输出命令`/home/skyeye-workspace/opt/skyeye/bin/skyeye`，启动skyeye了：

![](docs/readme/media/05.png)

****

# 更多教程

更多关于 Open-SkyEye 的使用教程，[请点击Wiki获取](https://open-skyeye.gitee.io/wiki)

学习 Open-SkyEye，请加QQ群：238612183

