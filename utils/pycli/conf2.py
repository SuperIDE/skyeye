import json
from collections import OrderedDict
from objects import Node, Root, Component, MethodNode, Machine, Cpu, Device, Linker
from skyeye_common_module import *

class ConfParser:
    def node_type(self, node):
        assert node
        if isinstance(node, dict):
            if 'base' not in node:
                ntype = 'root'
            else:
                ntype = 'component'
        elif isinstance(node, list):
            if 'iface' in node or all('iface' in child for child in node):
                ntype = 'iface' 
            else:
                ntype = 'attr' 
        else:
            ntype = 'value'

        return ntype

    def parse_root_node(self, node):
        node_type = self.node_type(node)
        assert node_type == 'root'

        child_list = list(node.items())
        return child_list

    def parse_component_node(self, node):
        node_type = self.node_type(node)
        assert node_type == 'component'

        base = node['base']
        cls  = node['class']
        child_list = []
        iface_list = []
        attr_list = []

        for name, child in node.items():
            node_type = self.node_type(child)
            if  node_type == 'component':
                child_list.append((name, child))
            elif  node_type == 'iface':
                iface_list.append((name, child))
            elif  node_type == 'attr':
                attr_list.append((name, child))

        return base, cls, child_list, iface_list, attr_list

class SkyeyeConf:
    # singleton pattern 
    _skyeye_conf_obj = None
    _config_done_callbacks = OrderedDict()
    _config_clear_callbacks = OrderedDict()

    @staticmethod
    def make_config(json_file_path):
        SkyeyeConf._skyeye_conf_obj = SkyeyeConf(json_file_path)
        SkyeyeConf.call_config_done_callbacks()

    @staticmethod
    def clear_config():
        SkyeyeConf._skyeye_conf_obj = None
        SkyeyeConf.call_config_clear_callbacks()

    @staticmethod
    def get_config():
        return SkyeyeConf._skyeye_conf_obj

    @staticmethod
    def add_config_done_callback(name, func):
        SkyeyeConf._config_done_callbacks[name] = func

    @staticmethod
    def del_config_done_callback(name):
        SkyeyeConf._config_done_callbacks.pop(name)

    @staticmethod
    def call_config_done_callbacks():
        for func in SkyeyeConf._config_done_callbacks.values():
            func()

    @staticmethod
    def add_config_clear_callback(name, func):
        SkyeyeConf._config_clear_callbacks[name] = func

    @staticmethod
    def del_config_clear_callback(name):
        SkyeyeConf._config_clear_callbacks.pop(name)

    @staticmethod
    def call_config_clear_callbacks():
        for func in SkyeyeConf._config_clear_callbacks.values():
            func()

    def __init__(self, json_file_path):
        self.machine_dict = OrderedDict()
        self.cpu_dict = OrderedDict()
        self.device_dict = OrderedDict()
        self.linker_dict = OrderedDict()
        self.components = OrderedDict()
        self.config_done_callbacks = OrderedDict()

        self.data = self.load_json_file(json_file_path)
        self.parser = ConfParser()

        self.build()

    def build(self):
        self.root = Root() 
        self.create_node(None, self.data, self.root)
        self.component_connect()
        self.component_set_attr()
        self.conf_commit()

    def load_json_file(self, json_path):
        try:
            with open(json_path) as f:
                return json.load(f, object_pairs_hook=OrderedDict)
        except Exception as e:
            raise e

    def create_node(self, name, node, parent):
        node_type = self.parser.node_type(node)
        if node_type == 'root':
            for name, component in self.parser.parse_root_node(node):
                self.create_node(name, component, parent)
        elif node_type == 'component':
            base, cls, child_list, iface_list, attr_list = \
                                    self.parser.parse_component_node(node)
            if base == 'mach':
                obj = self.create_mach(name, parent, base, cls, iface_list, attr_list)
            elif base == 'cpu':
                obj =self.create_cpu(name, parent, base, cls, iface_list, attr_list)
            elif base == 'device':
                obj = self.create_device(name, parent, base, cls, iface_list, attr_list)
            elif base == 'linker':
                obj = self.create_linker(name, parent, base, cls, iface_list, attr_list)

            for name, child in child_list: 
                self.create_node(name, child, obj)
        else:
            raise Exception('BUG!')

    def create_mach(self, name, parent, base, cls, iface_list, attr_list):
        mach = Machine(name, parent, base, cls, iface_list, attr_list)
        self.machine_dict[name] = mach
        self.components[name] = mach
        return mach

    def create_cpu(self, name, parent, base, cls, iface_list, attr_list):
        cpu = Cpu(name, parent, base, cls, iface_list, attr_list)
        self.cpu_dict[name] = cpu
        self.components[name] = cpu
        return cpu

    def create_device(self, name, parent, base, cls, iface_list, attr_list):
        device = Device(name, parent, base, cls, iface_list, attr_list)
        self.device_dict[name] = device
        self.components[name] = device
        return device

    def create_linker(self, name, parent, base, cls, iface_list, attr_list):
        linker = Linker(name, parent, base, cls, iface_list, attr_list)
        self.linker_dict[name] = linker
        self.components[name] = linker
        return linker

    def component_connect(self):
        for name, component in self.components.items():
            component.connect()

    def component_set_attr(self):
        for name, component in self.components.items():
            component.set_attr()

    def conf_commit(self):
        SkyEyeConfigConfObj()

    def get_object_tree(self):
        return self.root

    def get_machines(self):
        return self.machine_dict.copy()

    def get_cpus(self, machine=None):
        if machine is None:
            return self.cpu_dict.copy()

        mach_obj = self.machine_dict.get(machine, None)
        if mach_obj:
            return mach_obj.get_cpus()

        return None
    
    def get_devices(self, machine=None):
        if machine is None:
            return self.device_dict.copy()

        mach_obj = self.machine_dict.get(machine, None)
        if mach_obj:
            return mach_obj.get_devices()

        return None

    def get_linkers(self):
        return self.linker_dict.copy()

    def get_machine(self, name):
        return self.machine_dict.get(name, None)

    def get_cpu(self, name):
        return self.cpu_dict.get(name, None)

    def get_device(self, name):
        return self.device_dict.get(name, None)

    def get_linker(self, name):
        return self.linker_dict.get(name, None)

# API FUNC
make_config = SkyeyeConf.make_config
clear_config = SkyeyeConf.clear_config
get_config  = SkyeyeConf.get_config

add_config_done_callback = SkyeyeConf.add_config_done_callback
del_config_done_callback = SkyeyeConf.del_config_done_callback
add_config_clear_callback = SkyeyeConf.add_config_clear_callback
del_config_clear_callback = SkyeyeConf.del_config_clear_callback
