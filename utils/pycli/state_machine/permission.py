class Permission:
    always_allow = {
            'history', 'help', 
            'ls', 'cd', 
            'shell', 'run_py', 'run_pyfile',
            'quit', 'q', 
            'run_script',
            'list_modules', 'list_class', 'list_attr', 'list_connect', 'list_iface',
            'configure_read',
    }

class PermissionUnConfigured(Permission):
    allow = {'define_conf'}

class PermissionConfigured(Permission):
    allow = {
        'reset',
        'init_ok', 
        'load_binary', 'load_file',
        'md',
        'list_cpu', 'list_machine', 'list_device', 'list_register', 
    }

class PermissionStop(Permission):
    disallow = {'define_conf', 'init_ok'}

class PermissionRunning(Permission):
    allow = {
        'stop',
        'speed',
        'bp_list',
        'list_cpu', 'list_machine', 'list_device', 'list_register', 
    }
