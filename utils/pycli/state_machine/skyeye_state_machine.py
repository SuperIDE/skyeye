from abc import ABC, abstractproperty, abstractmethod
from exception import SkyeyeAPIException, ERROR_ALL_F
from .permission import PermissionUnConfigured, PermissionConfigured, PermissionStop, PermissionRunning

class State(ABC):
    name = ''
    prompt_string = ''

    @abstractmethod
    def transition(self, action):
        pass

    def check(self, action):
        if (action not in self.always_allow) and \
            ((hasattr(self, 'allow') and action not in self.allow) or \
             (hasattr(self, 'disallow') and action in self.disallow)):
            msg = 'state machine: Can\'t excute "%s" in %s.\n%s' % (action, self.name, self.prompt_string)
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

class UnConfiguredState(State, PermissionUnConfigured):
    name = 'UN_CONFIGURED_STATE'
    prompt_string = '(please try excuting "run-script" or "define-conf" first.)'

    def transition(self, action):
        if action == 'define_conf':
            return ConfiguredState.name 
        return self.name

class ConfiguredState(State, PermissionConfigured):
    name = 'CONFIGURED_STATE'
    prompt_string = '(please try excuting "init-ok" or "reset" first.)'

    def transition(self, action):
        if action == 'init_ok':
            return StopState.name 
        if action == 'reset':
            return UnConfiguredState.name
        return self.name

class StopState(State, PermissionStop):
    name = 'STOP_STATE'
    prompt_string = '(please try excuting "reset" first.)'

    def transition(self, action):
        if action == 'run':
            return RunningState.name

        if action == 'reset':
            return UnConfiguredState.name

        return self.name

class RunningState(State, PermissionRunning):
    name = 'RUNNING_STATE'
    prompt_string = '(please try excuting "stop" first.)'

    def transition(self, action):
        if action == 'stop':
            return StopState.name
        return self.name

class StateMachine:
    def __init__(self, state_cls_list, start_state_name):
        self.add_all_states(state_cls_list)
        self.current_state = self.states[start_state_name]

    def add_all_states(self, state_cls_list):
        self.states = {}
        for S in state_cls_list:
            self.states[S.name] = S()

    def check(self, action):
        self.current_state.check(action)

    def transition(self, action):
        state_name = self.current_state.transition(action)
        self.current_state = self.states[state_name]

state_cls_list = [UnConfiguredState, ConfiguredState, StopState, RunningState]
state_machine = StateMachine(state_cls_list, UnConfiguredState.name)
