from skyeye_common_module import *
import _thread
import time
from conf import *
import ctypes
import threading

CALLBACK_TYPE = CFUNCTYPE(None, ctypes.py_object)
WAIT_FUNC_TYPE = CFUNCTYPE(None)
NOTIFY_FUNC_TYPE = CFUNCTYPE(None)

class PyCallback:
    _instance = None

    @staticmethod
    def instance():
        if PyCallback._instance is None:
            PyCallback._instance = PyCallback()
        return PyCallback._instance

    def __init__(self):
        self.callback_current_id = 0
        self.callback_dict = {}

    def _create_wait_notify_func(self):
        event = threading.Event()
        def wait():
            event.wait()
        def notify():
            if not event.is_set():
                event.set()
        return wait, notify

    def create_timer_callback(self, cpu, ms, mode, callback, data):
        wait, notify = self._create_wait_notify_func()
        t = threading.Thread(target=self._thread_create_timer_callback, 
                        args=[cpu, ms, mode, callback, data, wait, notify])
        t.start()
        self.callback_current_id += 1
        self.callback_dict[self.callback_current_id] = notify
        return self.callback_current_id

    def _thread_create_timer_callback(self, cpu, ms, mode, callback, data, wait, notify):
        PythonTmrCreate(cpu, ms, mode, callback, data, wait, notify)

    def delete_timer_callback(self, callback_id):
        if callback_id not in self.callback_dict:
            print('PyCallback.delete_timer: No Timer ID is "%d"' % callback_id)
            return 

        notify = self.callback_dict.pop(callback_id)
        PythonTmrDelete(notify)

    def create_watch_pc_callback(self, cpu, pc_addr, callback, data):
        wait, notify = self._create_wait_notify_func()
        t = threading.Thread(target=self._thread_create_watch_pc_callback, 
                        args=[cpu, pc_addr, callback, data, wait, notify])
        t.start()
        self.callback_current_id += 1
        self.callback_dict[self.callback_current_id] = notify
        return self.callback_current_id

    def _thread_create_watch_pc_callback(self, cpu, pc_addr, callback, data, wait, notify):
        SkyEyeSetWatchOnPc(cpu, pc_addr, callback, data, wait, notify)

    def delete_watch_pc_callback(self, callback_id):
        if callback_id not in self.callback_dict:
            print('PyCallback.delete_watch_pc: No callback ID is "%d"' % callback_id)
            return 

        notify = self.callback_dict.pop(callback_id)
        SkyEyeUnWatchOnPc(notify)

    def create_watch_mm_callback(self, ms_name, mm_type, addr, data_type, length, callback, data):
        wait, notify = self._create_wait_notify_func()
        t = threading.Thread(target=self._thread_create_watch_mm_callback, 
                args=[ms_name, mm_type, addr, data_type, length, callback, data, wait, notify])
        t.start()
        self.callback_current_id += 1
        self.callback_dict[self.callback_current_id] = notify
        return self.callback_current_id

    def _thread_create_watch_mm_callback(self, ms_name, access_type, addr, data_type, length, 
                                                        callback, data, wait, notify):
        SkyEyeSetWatchOnMem(ms_name, access_type, addr, data_type, length, 
                                        callback, data, wait, notify)

    def delete_watch_mm_callback(self, callback_id):
        if callback_id not in self.callback_dict:
            print('PyCallback.delete_watch_mm: No callback ID is "%d"' % callback_id)
            return 

        notify = self.callback_dict.pop(callback_id)
        SkyEyeUnWatchOnMem(notify)
