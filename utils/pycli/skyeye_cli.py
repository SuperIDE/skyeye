﻿# -*- coding: UTF-8 -*-
import cmd, sys, time, platform
import errormessage
from exception import *
from conf import *
from skyeye_common_module import *
import win_workspace as ws
import tools
import cli_init
import cli
import _thread
from sky_log import *
from x86 import x86
import mips
import os
import se
import se_path
from se_system import system
import pytimer
import functools
import operator
import fault_inject as sfi
import argparse
import network_control as NetCtrl
import skyeye_command
import conf2

sys.path.append(se_path.SkyEyeBin)

DEBUG = 0
if DEBUG:
    import traceback

def add_help_method(cls):
    attrs = dir(cls)
    if 'print_help' not in attrs:
        raise NotImplementedError('No "print_help" method in class %s' % cls.__name__)

    def create_help_method(cmd):
        def help_method(self):
            self.print_help(cmd)
        return help_method

    cmds = [attr[3:] for attr in attrs if attr.startswith('do_')]
    for cmd in cmds:
        setattr(cls, 'help_'+cmd, create_help_method(cmd))

    return cls

def add_do_command_method(cls):
    def create_command_method(command):
        def do_xxx(self, arg):
            if not DEBUG:
                try:
                    command(arg=arg, cli=self)
                except Exception as e:
                    if str(e):
                        print('[ERROR]', e)
                except:
                    print('!!!BUG!!!')
            else:
                command(arg=arg, cli=self)

        return do_xxx

    def create_complete_method(command):
        def complete_xxx(self, text, line, begidx, endidx):
            return command.complete(text, line, begidx, endidx)
        return complete_xxx

    for name, command in skyeye_command._commands.items():
        if 'cli' in command.export:
            setattr(cls, 'do_'+name, create_command_method(command))
            if not getattr(cls, 'complete_'+name, None):
                setattr(cls, 'complete_'+name, create_complete_method(command))

    return cls

@add_help_method
@add_do_command_method
class SkyEyeCli(cmd.Cmd):
    # skyeye prompt
    prompt = '(skyeye) '

    def __init__(self, args=[]):

        cmd.Cmd.__init__(self)
        self.args_list = args
        self.print_welcome_message()
        # log=output_log()
        # se.log=log
        self.open_conf_flag = False
        #if "-gui" in self.args_list and tools.is_windows():
        #    from skyeye_gui import *
        #    win_control()
        RegisterLog(print)
        if(os.path.exists('.skyeyeinit')):
            tools.execute_skyeyeinit_file('.skyeyeinit')
        if tools.is_windows():
            ws.load_module_from_ws()
            try:
                NetCtrl.server_init()
            except:
                print ("网络服务启动失败,检查GRPC服务是否配置正确!")
        #pytimer.PyTmrInit()

        # MIPS
        mips.init()

        # 为命令补齐配置readline
        self.readline_cfg()

        # 命令参数解析器
        self.cmd_parsers = {}

        # 参数补齐时不截断形如xxx-yyy的命令
        self.identchars += '-/.'

        #conf callback
        self.register_config_callback()

    def print_welcome_message(self):
        os_info = platform.system()
        if operator.eq(os_info, "Linux"):
            system = "Linux"
        else:
            system = "Windows"

        try:
            config = tools.GetPackageConfig()
            version = tools.GetBuildVersion(config)
            date = tools.GetBuildDate(config)
        except:
            version = "unknown"
            date = "unknown"
        welcome_message = "Open-SkyEye %s <build %s %s> Copyright 2010-2022 Digiproto Corporation\n" % (version, date, system)
        print(welcome_message)

    def readline_cfg(self):
        import readline
        s = readline.get_completer_delims()
        for c in '<>-/':
            s = s.replace(c, '')
        #s += '.'
        readline.set_completer_delims(s)
        self.readline = readline

    def quit_skyeye(self, arg):
        libcommon.com_quit(arg)

    def cmdloop(self):
        try:
            super().cmdloop()
        except KeyboardInterrupt:
            self.do_quit('')

    def preloop(self):
        self.history_file = os.path.join(os.path.expanduser('~'), '.skyeye/cmd_history')
        if not os.path.exists(self.history_file):
            with open(self.history_file, 'w'):
                pass
        self.readline.read_history_file(self.history_file)

    def postloop(self):
        self.readline.write_history_file(self.history_file)

    def precmd(self, arg):
        if arg:
            if arg.startswith('%'):
                arg = 'run_py ' + arg[1:]

            arg_list = arg.split()
            if arg_list:
                arg_list[0] = arg_list[0].replace('-', '_')
            return ' '.join(arg_list)
        return arg

    def postcmd(self, stop, line):
        time.sleep(0.01)
        if libcommon.SIM_is_running():
            self.prompt = "(running) "
        else:
            self.prompt = "(skyeye) "

        return stop

    def print_help(self, cmd):
        cmd_obj = getattr(skyeye_command, cmd, None)
        if cmd_obj:
            cmd_obj.print_help()
        else:
            parser = self.get_argparser(cmd)
            parser.print_help()

    def completenames(self, text, *ignored):
        res1 = self.complete_obj_names(text)
        res2 = self.complete_command_names(text)
        return res1 + res2

    def complete_obj_names(self, text):
        res = []
        if self.config:
            root = self.config.get_object_tree()
            if '.' in text:
                p, txt = text.rsplit('.', 1)
            else:
                p, txt = '', text
            obj = root.find_obj_by_path(p)
            if obj:
                res = obj.match_cli_attr_name(txt)
                res = [p+'.'+item if p else item for item in res]
        return res

    def complete_command_names(self, text):
        if '-' in text:
            text = text.replace('-', '_')
        res = super().completenames(text)
        res = [s.replace('_', '-') for s in res]
        return res

    def completedefault(self, text, line, begidx, endidx):
        # TODO: 实现参数补齐功能
        cmd, args, foo = self.parseline(line)
        if '-' in cmd:
            cmd = cmd.replace('-', '_')
            compfunc = getattr(self, 'complete_'+cmd, None)
            if compfunc:
                return compfunc(text, line, begidx, endidx)
        return []

    def complete(self, text, state):
        return super().complete(text, state)

    def default(self, line):
        res = self.object_system_access(line)
        if not res:
            print('[Error] Unknown command: %s\n'%line)

    def object_system_access(self, line):
        if self.config:
            root = self.config.get_object_tree()
            cmd, arg, line = self.parseline(line)

            obj = root.find_obj_by_path(cmd)
            if obj:
                try:
                    obj(arg)
                except Exception as e:
                    if str(e):
                        print('[ERROR]', e)
                return True
        return False

    # Skyeye Config Object
    _skyeye_conf_obj = None

    def update_config(self):
        SkyEyeCli._skyeye_conf_obj = conf2.get_config()

    @property
    def config(self):
        return self._skyeye_conf_obj

    def register_config_callback(self):
        conf2.add_config_done_callback('cli_config_update', self.update_config)
        conf2.add_config_clear_callback('cli_config_update', self.update_config)
