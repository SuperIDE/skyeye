from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
from conf import GetGlobalConfig
import fault_inject as sfi
import tools
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='open_instr_record',
                description='configure_read',
                add_help=False)

        parser.add_argument(
                'cpu', 
                metavar='<cpu-name>',
                help='cpu name',
                )

        parser.add_argument(
                'log_file', 
                metavar='<log-file>',
                help='the log file path',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        if arg_ns.cpu not in config.get_cpus():
            raise SkyeyeAPIException(['0x40210004', arg_ns.cpu])
        print(arg_ns.cpu, arg_ns.log_file)
        try:
            ret = SkyEyeOpenInstrRecord(arg_ns.cpu, arg_ns.log_file)
        except Exception as e:
            raise SkyeyeAPIException(['0x40210000', e])
        if ret == 0:
            raise SkyeyeAPIException(['0x40212101', e])

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        cpus = config.get_cpus()
        return [item for item in cpus if item.startswith(text)]
