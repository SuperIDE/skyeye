from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='remote_gdb',
                description='Connect remote gdb.',
                add_help=False)

        parser.add_argument(
                'cpu',
                metavar='<cpu-name>',
                help='cpu name',
                )

        parser.add_argument(
                'ip', 
                metavar='<ip>',
                nargs='?',
                default='0.0.0.0',
                help='ip address',
                )

        parser.add_argument(
                'port', 
                metavar='<port>',
                type=convert_int,
                help='port number',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        try:
            ret = SkyEyeCreateRemoteGdb(arg_ns.cpu, arg_ns.port, arg_ns.ip)
        except:
            msg = "An error has occurred happend when remote gdb start!\n"
            msg += "Target CPU name: %s, Ip: %s, Port: %d" % (arg_ns.cpu, arg_ns.ip, arg_ns.port)
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        if ret != 1:
            msg = "Remote Gdb Start error, Target Cpu Name: %s, Ip: %s, Port: %d" % (arg_ns.cpu, arg_ns.ip, arg_ns.port)
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        print("Remote Gdb Start OK!")

        return False
