from . import SkyeyeCommand, convert_int
import argparse
import os
from exception import SkyeyeAPIException, ERROR_ALL_F
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='list_device',
                description='Show all current device.',
                add_help=False)

        parser.add_argument(
                'machine', 
                metavar='<machine-name>',
                nargs='?',
                help='machine name',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        machines = config.get_machines()
        print ("%-30s%-30s" % ("DeviceName", "BelongBoard"))
        if arg_ns.machine is None:
            n_mach = len(machines)
            for i, mach in enumerate(machines, 1):
                for device in config.get_devices(mach):
                    print  ("%-30s%-30s" % (device, mach))
                if i < n_mach:
                    print()
        else:
            devices =  config.get_devices(arg_ns.machine)
            if devices:
                for device in devices:
                    print  ("%-30s%-30s" % (device, arg_ns.machine))

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        ml = config.get_machines()
        return [item for item in ml if item.startswith(text)]
