from . import SkyeyeCommand, table_print
import argparse
import os
from exception import SkyeyeAPIException, ERROR_ALL_F
from skyeye_common_module import *

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='list_iface',
                description='Show the information of a class.',
                add_help=False)

        parser.add_argument(
                'cls', 
                metavar='<class-name>',
                help='class name',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        cls_l = SkyEyeGetClassList()
        if arg_ns.cls not in cls_l:
            msg = 'information IFACE "%s" was not found' % arg_ns.cls
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        iface_l = SkyEyeGetClassIfaceList(arg_ns.cls)
        iface_l.sort()
        table_print(iface_l)

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        cls_l = SkyEyeGetClassList()
        return [item for item in cls_l if item.startswith(text)]
