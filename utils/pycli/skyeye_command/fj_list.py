from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='fj_list',
                description='(xxxxxx)',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        ret = sfi.skyeye_get_fj()
        if ret is False:
            msg = 'Call sfi.skyeye_get_fj() Failed'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        columns = ("Device","Mach","Register","Address(HEX)", "Bit(DEC)", "Mode(DEC)")
        print ("%-20s%-20s%-20s%-20s%-10s%-10s" % columns)
        for fj in ret:
            #print ("%-20s%-20s%-20s%-20x%-10d%-10d" % (fj[0],fj[1],fj[2],fj[3],fj[4],fj[5]))
            print("%-20s%-20s%-20s%-20x%-10d%-10d" % tuple(fj))

        return False
