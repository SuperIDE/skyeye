from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='fj_set',
                description='(xxxxxx)',
                add_help=False)

        parser.add_argument(
                'machine', 
                metavar='<machine-name>',
                help='machine name',
                )

        parser.add_argument(
                'device', 
                metavar='<device-name>',
                help='device name',
                )

        parser.add_argument(
                'device_addr', 
                metavar='<device-addr>',
                type=convert_int,
                help='device addr',
                )

        parser.add_argument(
                'bit_index', 
                metavar='<bit-index>',
                type=convert_int,
                help='bit index',
                )

        parser.add_argument(
                'mode', 
                metavar='<mode>',
                type=convert_int,
                choices=range(3),
                help='mode',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        ret = sfi.skyeye_set_fj(arg_ns.machine, arg_ns.device, arg_ns.device_addr, arg_ns.bit_index, arg_ns.mode)
        if not ret:
            msg = 'Injection failure failed'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        print("Injection failure success")
        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        ml = config.get_machines()
        return [item for item in ml if item.startswith(text)]

    def complete_arg_2(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        mach = line.split()[1]
        devices = config.get_devices(mach)
        return [item for item in devices if item.startswith(text)]

    def complete_arg_3(self, text, line, begidx, endidx):
        if '0x'.startswith(text):
            return ['0x']
        return []

    def complete_arg_4(self, text, line, begidx, endidx):
        if begidx == endidx:
            return [str(i) for i in range(8)]
        return []

    def complete_arg_5(self, text, line, begidx, endidx):
        if begidx == endidx:
            return [str(i) for i in range(3)]
        return []
