from . import SkyeyeCommand, convert_int
import argparse
from skyeye_common_module import *

# Object common command
class ObjectCommand(SkyeyeCommand):
    need_check_state = False

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog=self.name,
                description='',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, obj=None):
        print(obj)
        return False

# Command of machine method
class Machine_list_cpu(SkyeyeCommand):
    need_check_state = False

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='Machine.list_cpu',
                description='Show all available cpu on the machine.',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, obj=None):
        print ("%-20s%-20s" % ("ID", "CpuName"))
        cpus = obj.get_cpus()
        for i, cpu in enumerate(cpus, 1):
            print  ("%-20d%-20s" % (i, cpu))

        return False

class Machine_list_device(SkyeyeCommand):
    need_check_state = False

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='Machine.list_device',
                description='Show all current device.',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, obj=None):
        print ("%-20s%-20s" % ("ID", "DeviceName"))
        devices = obj.get_devices()
        for i, device in enumerate(devices, 1):
            print  ("%-20d%-20s" % (i, device))

        return False

# Command of RegisterList
class RegisterListCommand(SkyeyeCommand):
    need_check_state = False

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog=self.name,
                description='',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, obj=None):
        print(obj)
        self.show_all_registers(obj)

    def show_all_registers(self, obj):
        print('%-25s%-25s%-15s' % ('register', 'value', 'address'))
        for reg in obj.cli_attrs.values():
            print('%-25s0x%-23x0x%-13x' % (reg.name, reg.value, reg.addr))
        return False

# Command of Register
class RegisterCommand(SkyeyeCommand):
    need_check_state = False

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog=self.name,
                description='',
                add_help=False)

        sub = parser.add_subparsers()
        sub_parser = sub.add_parser('=', add_help=False)
        sub_parser.add_argument(
                'value',
                metavar='<value>',
                type=convert_int,
                help='set a value to register.',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, obj=None):
        if 'value' in arg_ns:
            # reg = value
            self.set_register(obj, arg_ns.value)

        print(obj)
        self.show_register(obj)

    def set_register(sef, obj, value):
        device_name = obj.get_device_name()
        reg_id = SkyEyeGetDevRegIdByName(None, device_name, obj.name)
        SkyEyeSetDevRegValueById(None, device_name, value, reg_id)

    def show_register(self, obj):
        print('%-25s%-15s' % ("value", "address"))
        v = obj.value
        if v is not None:
            print('0x%-23x0x%-13x' % (v, obj.addr))
        else:
            print('%-25s0x%-13x' % (v, obj.addr))
