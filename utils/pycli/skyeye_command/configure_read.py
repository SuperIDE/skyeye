from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
from conf import GetGlobalConfig
import fault_inject as sfi
from conf import *
import tools

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='configure_read',
                description='configure_read',
                add_help=False)

        parser.add_argument(
                'snapshot', 
                metavar='<snapshot>',
                help='snapshot',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = GetGlobalConfig()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        t_cwd = os.getcwd()
        try:
            full_dir = tools.cmd_get_chp_dir(arg_ns.snapshot)
        except IOError as e:
            raise SkyeyeAPIException(['0x40110000', e.message])

        os.chdir(full_dir)
        try:
            tools.start_ckpt_config(full_dir)
        except IOError as e:
            raise SkyeyeAPIException(['0x40110000', e.message])

        config = GetGlobalConfig()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        mach_list = config.get_mach_list()
        for mach in mach_list:
            cpu = config.get_cpuname_by_mach(mach)
            cls = config.get_classname(cpu)
            if cls == "x86":
                SkyEyeX86LoadConfigure(cpu, full_dir)

        info_dic = tools.ckpt_get_image_info(full_dir)
        images = info_dic.keys()
        for image in images:
            for page_l in info_dic[image]:
                SkyEyeImageFileToPage(image, page_l[0], page_l[1])

        config = os.path.join(full_dir, "config")
        SkyEyeRecoverConfigure(config)
        os.chdir(t_cwd)

        return False
