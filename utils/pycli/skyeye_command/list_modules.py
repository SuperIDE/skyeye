from . import SkyeyeCommand
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='list_modules',
                description='List all the loaded module.',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        SkyEyeListModules()
        return False
