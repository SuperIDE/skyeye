from . import SkyeyeCommand
import argparse
import os
import conf2
from exception import SkyeyeAPIException, ERROR_ALL_F

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='list_machines',
                description='List all the supported machines for SkyEye.',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        print("%-20s%-20s" % ("Id", "MachinesName"))
        for i, mach in enumerate(config.get_machines(), 1):
            print("%-20s%-20s" % (i, mach))

        return False
