from . import SkyeyeCommand, table_print
import argparse
import os
from exception import SkyeyeAPIException, ERROR_ALL_F
from skyeye_common_module import *

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='list_class',
                description='Show all classes.',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        cls_l = SkyEyeGetClassList()
        cls_l.sort()
        table_print(cls_l)

        return False
