from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi
import pytimer
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='stepi',
                description='Step one instruction exactly.',
                add_help=False)

        parser.add_argument(
                'cpu', 
                metavar='<cpu-name>',
                help='cpu name',
                )

        parser.add_argument(
                'size', 
                metavar='<step-size>',
                type=convert_int,
                help='step size',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        SkyEyeStepi(arg_ns.cpu, str(arg_ns.size))

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        cpus = config.get_cpus()
        return [item for item in cpus if item.startswith(text)]
