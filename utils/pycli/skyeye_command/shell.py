from . import SkyeyeCommand
import argparse
from exception import SkyeyeAPIException, ERROR_ALL_F
from skyeye_common_module import *
import subprocess
import shlex

class Command(SkyeyeCommand):
    export = ['cli']
    need_check_arg = False

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='shell',
                description='run a shell command',
                add_help=False)

        parser.add_argument(
                'shell_cmd', 
                metavar='<shell-cmd>',
                default='',
                help='shell command',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        if arg:
            try:
                subprocess.run(shlex.split(arg))
            except:
                import traceback
                raise SkyeyeAPIException([ERROR_ALL_F, traceback.format_exc()])

        return False
