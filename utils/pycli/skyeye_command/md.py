from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import fault_inject as sfi
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='md',
                description='Get the value at a address of memory.',
                add_help=False)

        parser.add_argument(
                'cpu',
                metavar='<cpu-name>',
                help='cpu name',
                )

        parser.add_argument(
                'addr', 
                metavar='<addr>',
                type=convert_int,
                help='memory address',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        ret = SkyEyeReadMemory8(arg_ns.cpu, arg_ns.addr)
        print ("%-20s%-20s" % ("Addr(HEX)", "Value(HEX)"))
        print ("%-20x%-20.2x" % (arg_ns.addr, ret))

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        cpus = config.get_cpus()
        return [item for item in cpus if item.startswith(text)]

    def complete_arg_2(self, text, line, begidx, endidx):
        if '0x'.startswith(text):
            return ['0x']
        return []
