from . import SkyeyeCommand, convert_int
import argparse
import os
from exception import SkyeyeAPIException, ERROR_ALL_F
import conf2
from skyeye_common_module import SkyEyeLoadFile

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog=' load_file',
                description='load file',
                add_help=False)

        parser.add_argument(
                'cpu', 
                metavar='<cpu-name>',
                help='name of cpu',
                )

        parser.add_argument(
                'path', 
                metavar='<file-path>',
                help='file path',
                )
        
        parser.add_argument(
                'addr', 
                metavar='<address>',
                type=convert_int,
                help='address in memory',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        if script:
            script_dir = os.path.dirname(script)
            path = os.path.join(script_dir, arg_ns.path)
        else:
            path = arg_ns.path

        #config = GetGlobalConfig()
        config = conf2.get_config()

        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        if arg_ns.cpu not in config.get_cpus():
            raise SkyeyeAPIException(['0x40030004', arg_ns.cpu])

        if not os.path.exists(path):
            raise SkyeyeAPIException(['0x40030002', arg_ns.path])

        SkyEyeLoadFile(arg_ns.cpu, path, arg_ns.addr)
        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        cpus = config.get_cpus()
        return [item for item in cpus if item.startswith(text)]

    def complete_arg_2(self, text, line, begidx, endidx):
        dir_name, last = os.path.split(text)
        top = dir_name if dir_name else '.'
        _, dirs, files, _ = next(os.fwalk(top))
        if last == '.':
            dirs += ['.', '..']
        elif last == '..':
            dirs += ['..']
        dirs = [(os.path.join(dir_name, item))+'/' for item in dirs]
        files = [(os.path.join(dir_name, item)) for item in files]
        items = files + dirs
        return [item for item in items if item.startswith(text)]

    def complete_arg_3(self, text, line, begidx, endidx):
        if '0x'.startswith(text):
            return ['0x']
        return []
