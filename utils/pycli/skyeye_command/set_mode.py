from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='set_mode',
                description='Set the cpu running mode',
                add_help=False)

        parser.add_argument(
                'mode', 
                metavar='<mode>',
                type=convert_int,
                choices=range(4),
                help='mode (choose from 0, 1, 2, 3)',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])
        print ("--%d--" % arg_ns.mode)
        cpus = config.get_cpus()
        for cpu in cpus:
            SkyEyeCpuSetMode(cpu, arg_ns.mode)

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        if begidx == endidx:
            return [str(i) for i in range(4)]
        return []
