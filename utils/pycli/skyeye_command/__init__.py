from importlib import import_module
import os
import sys
if __name__ == 'skyeye_command':
    from ._base_ import *
else:
    from .. import *

def load_one_command(this, name):
    m = import_module('.'+name, __name__)
    command = m.Command(name)
    this._commands[name] = command

def load_package(this, name):
    m = import_module('.'+name, __name__)
    this._commands.update(m._commands)

def load_all_commands(this):
    path, _ = os.path.split(__file__)
    _, dirs, files = next(os.walk(path))

    for d in dirs:
        if d[0] != '_':
            m = load_package(this, d)

    for f in files:
        if f[0] != '_' and f[-3:] == '.py':
            name = f[:-3]
            load_one_command(this, name)

    if __name__ == 'skyeye_command':
        for name, command in this._commands.items(): 
            setattr(this, name, command)

def get_all_command_name():
    return list(this_module._commands.keys())

this_module = import_module(__name__)
this_module._commands = {}
load_all_commands(this_module)

