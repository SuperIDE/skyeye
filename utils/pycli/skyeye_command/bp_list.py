from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import SkyEyeGetBpNumbers, SkyEyeGetBreakpointAddrById
from exception import SkyeyeAPIException, ERROR_ALL_F
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='bp_list',
                description='Show all break-points.',
                add_help=False)
        parser.add_argument(
                'cpu', 
                metavar='<cpu-core>',
                help='cpu-core name',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        temp = []
        n = SkyEyeGetBpNumbers(arg_ns.cpu)
        for i in range(n):
            temp.append(SkyEyeGetBreakpointAddrById(arg_ns.cpu, i))

        print("%-25s%-25s" % ("CPU","Address(HEX)"))
        for i in temp:
            print("%-25s0x%-25x" % (arg_ns.cpu, i))

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        config = conf2.get_config()
        if not config:
            return []
        cpus = config.get_cpus()
        return [item for item in cpus if item.startswith(text)]
