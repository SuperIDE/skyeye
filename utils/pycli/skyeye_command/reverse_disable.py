from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F
import tools
import os
import sys
from conf import *
import traceback
import shutil
from se_path import InstallDir

import errormessage
from exception import *
import traceback
import shutil
from se_path import InstallDir
import conf2

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='reverse_disable',
                description='reverse_disable',
                add_help=False)

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        config = conf2.get_config()
        if not config:
            msg = 'Can\'t get the config.'
            raise SkyeyeAPIException([ERROR_ALL_F, msg])

        if not GetReverseState():
            raise SkyeyeAPIException(['0x40066001'])

        SetReverseState(False)

        return False
