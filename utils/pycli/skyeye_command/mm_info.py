from . import SkyeyeCommand, convert_int
import argparse
import os
from skyeye_common_module import *
from exception import SkyeyeAPIException, ERROR_ALL_F

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='mm_info',
                description='mm_info',
                add_help=False)

        parser.add_argument(
                'func', 
                nargs='?',
                default='',
                metavar='<func-name>',
                help='function name',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        SkyEyeMemoryInfo(arg_ns.func)

        return False

    def complete_arg_1(self, text, line, begidx, endidx):
        funcs = [
                'mm', 'mm_zero', 'strdup', 'mm_align',
                'align_free', 'free', 'realloc',
                ]

        return [item for item in funcs if item.startswith(text)]
