from . import SkyeyeCommand
import argparse
import os
import cli
from tools import SkyEyeSetScriptPath
from exception import SkyeyeAPIException, ERROR_ALL_F
import project_config as pc
from importlib import import_module, reload

class Command(SkyeyeCommand):
    export = ['cli']

    def create_argparser(self):
        parser = argparse.ArgumentParser(
                prog='run_script',
                description='Run skyeye script.',
                add_help=False)

        parser.add_argument(
                'path', 
                metavar='<skyeye-script>',
                help='name of skyeye-script',
                )

        return parser
    
    def call(self, arg='', arg_ns=None, cli=None, script=None, **meta):
        full_file_name = os.path.realpath(arg_ns.path)
        if not os.path.exists(arg_ns.path):
            raise SkyeyeAPIException(['0x40160002', arg_ns.path])

        SkyEyeSetScriptPath(full_file_name)
        commands = self._get_commands_from_script(full_file_name)
        for command, arg in commands:
            command(arg=arg, cli=cli, script=full_file_name)
        
        return False

    def _get_commands_from_script(self, path):
        skyeye_command = import_module('skyeye_command')
        commands = []
        with open(path) as f:
            for line in f:
                line = line.strip()
                if line and not line.startswith('#'):
                    name_and_arg = line.split(maxsplit=1)
                    if len(name_and_arg) == 1:
                        name_and_arg.append('')
                    name, arg = name_and_arg
                    name = name.replace('-', '_')
                    command = getattr(skyeye_command, name, None)
                    if command is None:
                        msg = 'Unknown command: %s' % name
                        raise SkyeyeAPIException([ERROR_ALL_F, msg])
                    commands.append((command, arg))
        return commands

    def complete_arg_1(self, text, line, begidx, endidx):
        dir_name, last = os.path.split(text)
        top = dir_name if dir_name else '.'
        _, dirs, files, _ = next(os.fwalk(top))
        if last == '.':
            dirs += ['.', '..']
        elif last == '..':
            dirs += ['..']
        dirs = [(os.path.join(dir_name, item))+'/' for item in dirs]
        files = [(os.path.join(dir_name, item)) for item in files 
                            if os.path.splitext(item)[1] == '.skyeye']
        items = files + dirs
        return [item for item in items if item.startswith(text)]
