#include <windows.h>

#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include "config.h"

#if 0
#include <config.h>
#include <bfd.h>
#include <psapi.h>
#include <imagehlp.h>

#define BUFFER_MAX (16*1024)

#define BFD_ERR_OK          (0)
#define BFD_ERR_OPEN_FAIL   (1)
#define BFD_ERR_BAD_FORMAT  (2)
#define BFD_ERR_NO_SYMBOLS  (3)
#define BFD_ERR_READ_SYMBOL (4)

static const char *const bfd_errors[] = {
    "",
    "(Failed to open bfd)",
    "(Bad format)",
    "(No symbols)",
    "(Failed to read symbols)",
};

struct bfd_ctx
{
    bfd *handle;
    asymbol **symbol;
};

struct bfd_set
{
    char *name;
    struct bfd_ctx *bc;
    struct bfd_set *next;
};

struct find_info
{
    asymbol **symbol;
    bfd_vma counter;
    const char *file;
    const char *func;
    unsigned line;
};

struct output_buffer
{
    char *buf;
    size_t sz;
    size_t ptr;
};

static void output_init(struct output_buffer *ob, char *buf, size_t sz)
{
    ob->buf = buf;
    ob->sz = sz;
    ob->ptr = 0;
    ob->buf[0] = '\0';
}

static void output_print(struct output_buffer *ob, const char *format, ...)
{
    if (ob->sz == ob->ptr)
        return;
    ob->buf[ob->ptr] = '\0';
    va_list ap;

    va_start(ap, format);
    vsnprintf(ob->buf + ob->ptr, ob->sz - ob->ptr, format, ap);
    va_end(ap);

    ob->ptr = strlen(ob->buf + ob->ptr) + ob->ptr;
}

static void lookup_section(bfd * abfd, asection * sec, void *opaque_data)
{
    struct find_info *data = opaque_data;

    if (data->func)
        return;

    if (!(bfd_get_section_flags(abfd, sec) & SEC_ALLOC))
        return;

    bfd_vma vma = bfd_get_section_vma(abfd, sec);

    if (data->counter < vma || vma + bfd_get_section_size(sec) <= data->counter)
        return;

    bfd_find_nearest_line(abfd, sec, data->symbol, data->counter - vma, &(data->file), &(data->func), &(data->line));
}

static DWORD PEGetImageBase(PBYTE lpFileBase)
{
    PIMAGE_DOS_HEADER pDosHeader;
    PIMAGE_NT_HEADERS pNtHeaders;
    PIMAGE_OPTIONAL_HEADER pOptionalHeader;
    PIMAGE_OPTIONAL_HEADER32 pOptionalHeader32;
    PIMAGE_OPTIONAL_HEADER64 pOptionalHeader64;
    DWORD ImageBase = 0;

    pDosHeader = (PIMAGE_DOS_HEADER) lpFileBase;

    pNtHeaders = (PIMAGE_NT_HEADERS) (lpFileBase + pDosHeader->e_lfanew);

    pOptionalHeader = &pNtHeaders->OptionalHeader;
    pOptionalHeader32 = (PIMAGE_OPTIONAL_HEADER32) pOptionalHeader;
    pOptionalHeader64 = (PIMAGE_OPTIONAL_HEADER64) pOptionalHeader;

    switch (pOptionalHeader->Magic)
    {
        case IMAGE_NT_OPTIONAL_HDR32_MAGIC:
            ImageBase = pOptionalHeader32->ImageBase;
            break;
        case IMAGE_NT_OPTIONAL_HDR64_MAGIC:
            ImageBase = pOptionalHeader64->ImageBase;
            break;
    }

    return ImageBase;
}

static uint32_t virtual_address_to_module_address(const char *module_file, DWORD va, DWORD * ma)
{
    HANDLE hFile;
    HANDLE hFileMapping;
    PBYTE lpFileBase;
    DWORD image_base_vma;
    DWORD module_base;

    hFile = CreateFileA(module_file, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

    if (hFile == INVALID_HANDLE_VALUE)
    {
        *ma = va;
        return 0;
    }

    hFileMapping = CreateFileMapping(hFile, NULL, PAGE_READONLY, 0, 0, NULL);
    if (!hFileMapping)
    {
        CloseHandle(hFile);
        *ma = va;
        return 0;
    }

    lpFileBase = (PBYTE) MapViewOfFile(hFileMapping, FILE_MAP_READ, 0, 0, 0);
    if (!lpFileBase)
    {
        CloseHandle(hFileMapping);
        CloseHandle(hFile);
        *ma = va;
        return 0;
    }
    // Get PE File Image Base Addr
    image_base_vma = PEGetImageBase(lpFileBase);

    // Get Moudle Base Addr
    HMODULE hModule = NULL;
    BOOL bRet = GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
                                   GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
                                   (LPCSTR) (UINT_PTR) va,
                                   &hModule);

    if (!bRet)
    {
        *ma = va;
        return 0;
    }

    module_base = (DWORD) (UINT_PTR) hModule;

    // Calc module address
    *ma = image_base_vma + va - module_base;

    CloseHandle(hFileMapping);
    CloseHandle(hFile);
    return 1;
}

static void
find(struct bfd_ctx *b, DWORD offset, const char *module_file, const char **file, const char **func, unsigned *line)
{
    DWORD module_offset = 0;

    if (module_file != NULL)
    {
        int ret = virtual_address_to_module_address(module_file, offset, &module_offset);

        if (ret == 0)
        {
            char line_buffer[sizeof (IMAGEHLP_LINE) + 255];
            IMAGEHLP_LINE *lineInfo = (IMAGEHLP_LINE *) line_buffer;

            lineInfo->SizeOfStruct = sizeof (lineInfo);
            DWORD dwLineDisplacement = 0;

            if (SymGetLineFromAddr(GetCurrentProcess(), offset, &dwLineDisplacement, lineInfo))
            {
                *file = lineInfo->FileName;
                *line = lineInfo->LineNumber;
            }
        }
    }

    struct find_info data;

    data.func = NULL;
    data.symbol = b->symbol;
    data.counter = module_offset;
    data.file = NULL;
    data.func = NULL;
    data.line = 0;

    bfd_map_over_sections(b->handle, &lookup_section, &data);
    if (file)
    {
        *file = data.file;
    }
    if (func)
    {
        *func = data.func;
    }
    if (line)
    {
        *line = data.line;
    }
}

static int init_bfd_ctx(struct bfd_ctx *bc, const char *procname, int *err)
{
    bc->handle = NULL;
    bc->symbol = NULL;

    bfd *b = bfd_openr(procname, 0);

    if (!b)
    {
        if (err)
        {
            *err = BFD_ERR_OPEN_FAIL;
        }
        return 1;
    }

    if (!bfd_check_format(b, bfd_object))
    {
        bfd_close(b);
        if (err)
        {
            *err = BFD_ERR_BAD_FORMAT;
        }
        return 1;
    }

    if (!(bfd_get_file_flags(b) & HAS_SYMS))
    {
        bfd_close(b);
        if (err)
        {
            *err = BFD_ERR_NO_SYMBOLS;
        }
        return 1;
    }

    void *symbol_table;

    unsigned dummy = 0;

    if (bfd_read_minisymbols(b, FALSE, &symbol_table, &dummy) == 0)
    {
        if (bfd_read_minisymbols(b, TRUE, &symbol_table, &dummy) < 0)
        {
            free(symbol_table);
            bfd_close(b);
            if (err)
            {
                *err = BFD_ERR_READ_SYMBOL;
            }
            return 1;
        }
    }

    bc->handle = b;
    bc->symbol = symbol_table;

    if (err)
    {
        *err = BFD_ERR_OK;
    }
    return 0;
}

static void close_bfd_ctx(struct bfd_ctx *bc)
{
    if (bc)
    {
        if (bc->symbol)
        {
            free(bc->symbol);
        }
        if (bc->handle)
        {
            bfd_close(bc->handle);
        }
    }
}

static struct bfd_ctx *get_bc(struct bfd_set *set, const char *procname, int *err)
{
    while (set->name)
    {
        if (strcmp(set->name, procname) == 0)
        {
            return set->bc;
        }
        set = set->next;
    }
    struct bfd_ctx bc;

    if (init_bfd_ctx(&bc, procname, err))
    {
        return NULL;
    }
    set->next = calloc(1, sizeof (*set));
    set->bc = malloc(sizeof (struct bfd_ctx));
    memcpy(set->bc, &bc, sizeof (bc));
    set->name = strdup(procname);

    return set->bc;
}

static void release_set(struct bfd_set *set)
{
    while (set)
    {
        struct bfd_set *temp = set->next;

        free(set->name);
        close_bfd_ctx(set->bc);
        free(set);
        set = temp;
    }
}

static int dumpSourceCode(struct output_buffer *ob, LPCSTR lpFileName, DWORD dwLineNumber)
{
    FILE *fp;
    unsigned i;
    char szFileName[MAX_PATH] = "";
    DWORD dwContext = 2;

    if (lpFileName[0] == '/' && lpFileName[1] == '/')
    {
        szFileName[0] = lpFileName[2];
        szFileName[1] = ':';
        strcpy(szFileName + 2, lpFileName + 3);
    } else
    {
        strcpy(szFileName, lpFileName);
    }

    if ((fp = fopen(szFileName, "r")) == NULL)
    {
        return 0;
    }

    i = 0;
    while (!feof(fp) && ++i <= dwLineNumber + dwContext)
    {
        int c;

        if ((int) i >= (int) dwLineNumber - (int) dwContext)
        {
            output_print(ob, i == dwLineNumber ? ">%5i: " : "%6i: ", i);
            while (!feof(fp) && (c = fgetc(fp)) != '\n')
            {
                output_print(ob, "%c", c);
            }
            output_print(ob, "\n");
        } else
        {
            while (!feof(fp) && (c = fgetc(fp)) != '\n')
                ;
        }
    }

    fclose(fp);
    return 1;
}

static void _backtrace(struct output_buffer *ob, struct bfd_set *set, int depth, LPCONTEXT context)
{
    char procname[MAX_PATH];

    GetModuleFileNameA(NULL, procname, sizeof procname);

    struct bfd_ctx *bc = NULL;
    int err = BFD_ERR_OK;

    STACKFRAME frame;

    memset(&frame, 0, sizeof (frame));

    frame.AddrPC.Offset = context->Eip;
    frame.AddrPC.Mode = AddrModeFlat;
    frame.AddrStack.Offset = context->Esp;
    frame.AddrStack.Mode = AddrModeFlat;
    frame.AddrFrame.Offset = context->Ebp;
    frame.AddrFrame.Mode = AddrModeFlat;

    HANDLE process = GetCurrentProcess();
    HANDLE thread = GetCurrentThread();

    char symbol_buffer[sizeof (IMAGEHLP_SYMBOL) + 255];
    char module_name_raw[MAX_PATH];

    output_print(ob, "AddrPC   Params\n");

    while (StackWalk(IMAGE_FILE_MACHINE_I386,
                     process, thread, &frame, context, 0, SymFunctionTableAccess, SymGetModuleBase, 0))
    {

        --depth;
        if (depth < 0)
            break;

        IMAGEHLP_SYMBOL *symbol = (IMAGEHLP_SYMBOL *) symbol_buffer;

        symbol->SizeOfStruct = (sizeof *symbol) + 255;
        symbol->MaxNameLength = 254;

        DWORD module_base = SymGetModuleBase(process, frame.AddrPC.Offset);

        const char *module_name = "[unknown module]";

        if (module_base && GetModuleFileNameA((HINSTANCE) module_base, module_name_raw, MAX_PATH))
        {
            module_name = module_name_raw;
            bc = get_bc(set, module_name, &err);
        }

        const char *file = NULL;
        const char *func = NULL;
        unsigned line = 0;

        if (bc)
        {
            const char *find_module_name = NULL;

            if (module_base != 0)
            {
                find_module_name = module_name_raw;
            }
            find(bc, frame.AddrPC.Offset, find_module_name, &file, &func, &line);
        }

        int is_have_file = 1;

        if (file == NULL)
        {
            is_have_file = 0;
            DWORD dummy = 0;

            if (SymGetSymFromAddr(process, frame.AddrPC.Offset, &dummy, symbol))
            {
                file = symbol->Name;
            } else
            {
                file = "[unknown file]";
            }
        }

        output_print(ob,
                     "%08lX %08lX %08lX %08lX : ",
                     (DWORD) frame.AddrPC.Offset,
                     (DWORD) frame.Params[0], (DWORD) frame.Params[1], (DWORD) frame.Params[2]);

        if (func == NULL)
        {
            output_print(ob, "%s : %s %s\n", module_name, file, bfd_errors[err]);
        } else
        {
            output_print(ob, "%s : %s (%d) : in function (%s) \n", module_name, file, line, func);
        }

        if (is_have_file)
        {
            dumpSourceCode(ob, file, line);
        }
    }
}

static char *g_output = NULL;
static LPTOP_LEVEL_EXCEPTION_FILTER g_prev = NULL;

static LONG WINAPI exception_filter(LPEXCEPTION_POINTERS info)
{
    struct output_buffer ob;

    output_init(&ob, g_output, BUFFER_MAX);

    char szMsg[1024] = { 0 };
    sprintf(szMsg, "Error Code   : 0x%08x\n"
            "Error Address: 0x%08x\n"
            "CPU Register :\n"
            "EAX=0x%08x, EBX=0x%08x, ECX=0x%08x, EDX=0x%08x\n"
            "EBP=0x%08x, ESI=0x%08x, EDI=0x%08x, ESP=0x%08x",
            info->ExceptionRecord->ExceptionCode,
            info->ExceptionRecord->ExceptionAddress,
            info->ContextRecord->Eax, info->ContextRecord->Ebx,
            info->ContextRecord->Ecx, info->ContextRecord->Edx,
            info->ContextRecord->Ebp, info->ContextRecord->Esi, info->ContextRecord->Edi, info->ContextRecord->Esp);
    output_print(&ob, "%s\n\n", szMsg);

    if (!SymInitialize(GetCurrentProcess(), 0, TRUE))
    {
        output_print(&ob, "Failed to init symbol context\n");
    } else
    {
        bfd_init();
        struct bfd_set *set = calloc(1, sizeof (*set));

        _backtrace(&ob, set, 128, info->ContextRecord);
        release_set(set);

        SymCleanup(GetCurrentProcess());
    }

    int l_i_tmp_len;
    char sysTempPath[MAX_PATH + 1];
    char tempFileName[MAX_PATH + 1];

    l_i_tmp_len = GetTempPath(MAX_PATH, sysTempPath);

    // Symbol conversion '\\' to '/'
    char *p;

    p = sysTempPath;
    while (*p)
    {
        if (*p == '\\')
            *p = '/';
        p++;
    }

    strncpy(sysTempPath + l_i_tmp_len, "skyeye_temp", MAX_PATH - l_i_tmp_len);

    // Create temp dir
    if (_access(sysTempPath, F_OK) != 0)
    {
        if (_mkdir(sysTempPath) != 0)
        {
            fprintf(stderr, "Can not create skyeye_temp path: %s\n", sysTempPath);
            return 0;
        }
    }

    time_t now_time = time(0);
    struct tm *ltm = localtime(&now_time);
    char curr_time[64];

    sprintf(curr_time, "%d%02d%02d%02d%02d%02d",
            1900 + ltm->tm_year, 1 + ltm->tm_mon, ltm->tm_mday, ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
    sprintf(tempFileName, "%s/skyeye_bugreport_%s.log", sysTempPath, curr_time);

    fputs(g_output, stderr);
    FILE *output_logfile;

    output_logfile = fopen(tempFileName, "wb+");
    fputs(g_output, output_logfile);
    fflush(output_logfile);
    fclose(output_logfile);

    char cmdLine[256];

    sprintf(cmdLine, "bugreport.exe -f %s -t %s", tempFileName, curr_time);

    STARTUPINFO si;

    ZeroMemory(&si, sizeof (si));
    si.cb = sizeof (si);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_SHOW;
    PROCESS_INFORMATION pi;

    ZeroMemory(&pi, sizeof (pi));
    if (CreateProcess(NULL,             // name of executable module
                      cmdLine,          // command line string
                      NULL,             // process attributes
                      NULL,             // thread attributes
                      FALSE,            // handle inheritance option
                      0,                // creation flags
                      NULL,             // new environment block
                      NULL,             // current directory name
                      &si,              // startup information
                      &pi))             // process information
    {
        CloseHandle(pi.hThread);
        CloseHandle(pi.hProcess);
    }
    exit(1);

    return 0;
}

// Printing exception information in the debugger's console windows
#define DBG_PRINTEXCEPTION_C         0x40010006
#define DBG_PRINTEXCEPTION_WIDE_C    0x4001000A

LONG WINAPI VectoredExceptionHandler(PEXCEPTION_POINTERS pExceptionInfo)
{
    // The following exceptions will be triggered
    switch (pExceptionInfo->ExceptionRecord->ExceptionCode)
    {
        case EXCEPTION_ACCESS_VIOLATION:       // 0xC0000005
        case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:  // 0xC000008C
        case EXCEPTION_BREAKPOINT:     // 0x80000003
        case EXCEPTION_DATATYPE_MISALIGNMENT:  // 0x80000002
        case EXCEPTION_FLT_DENORMAL_OPERAND:   // 0xC000008D
        case EXCEPTION_FLT_DIVIDE_BY_ZERO:     // 0xC000008E
        case EXCEPTION_FLT_INEXACT_RESULT:     // 0xC000008F
        case EXCEPTION_FLT_INVALID_OPERATION:  // 0xC0000090
        case EXCEPTION_FLT_OVERFLOW:   // 0xC0000091
        case EXCEPTION_FLT_STACK_CHECK:        // 0xC0000092
        case EXCEPTION_FLT_UNDERFLOW:  // 0xC0000093
        case EXCEPTION_ILLEGAL_INSTRUCTION:    // 0xC000001D
        case EXCEPTION_IN_PAGE_ERROR:  // 0xC0000006
        case EXCEPTION_INT_DIVIDE_BY_ZERO:     // 0xC0000094
        case EXCEPTION_INT_OVERFLOW:   // 0xC0000095
        case EXCEPTION_INVALID_DISPOSITION:    // 0xC0000026
        case EXCEPTION_NONCONTINUABLE_EXCEPTION:       // 0xC0000025
        case EXCEPTION_PRIV_INSTRUCTION:       // 0xC0000096
        case EXCEPTION_SINGLE_STEP:    // 0x80000004
        case EXCEPTION_STACK_OVERFLOW: // 0xC00000FD
            exception_filter(pExceptionInfo);
            break;
        case DBG_PRINTEXCEPTION_C:
        case DBG_PRINTEXCEPTION_WIDE_C:
            break;
        default:
            fprintf(stderr, "Unrecognized ExceptionCode: 0x%x\n", pExceptionInfo->ExceptionRecord->ExceptionCode);
    }

    return EXCEPTION_CONTINUE_SEARCH;
}

void backtrace_register(void)
{
    if (g_output == NULL)
    {
        g_output = malloc(BUFFER_MAX);
        g_prev = SetUnhandledExceptionFilter(exception_filter);
        AddVectoredExceptionHandler(1, VectoredExceptionHandler);
    }
}

void backtrace_unregister(void)
{
    if (g_output)
    {
        free(g_output);
        SetUnhandledExceptionFilter(g_prev);
        AddVectoredExceptionHandler(1, g_prev);
        g_prev = NULL;
        g_output = NULL;
    }
}
#else

#include "exchndl.h"

HMODULE g_hDll = NULL;
void (*ExcHndlInit_func) () = NULL;

BOOL(*ExcHndlSetLogFileNameA_func) () = NULL;
BOOL(*ExcHndlSetExceptionCallBack_func) (ExceptionCallBack_t cbBeforeFunc, ExceptionCallBack_t cbAfterFunc) = NULL;
#define CallFunction(func, ...) if (func) func(__VA_ARGS__);

static char tempFileName[MAX_PATH + 1];
static char curr_time[64];

void ExceptionBeforeCallBack()
{
    int l_i_tmp_len;
    char sysTempPath[MAX_PATH + 1];

    l_i_tmp_len = GetTempPath(MAX_PATH, sysTempPath);

    // Symbol conversion '\\' to '/'
    char *p;

    p = sysTempPath;
    while (*p)
    {
        if (*p == '\\')
            *p = '/';
        p++;
    }

    strncpy(sysTempPath + l_i_tmp_len, "skyeye_temp", MAX_PATH - l_i_tmp_len);

    // Create temp dir
    if (_access(sysTempPath, F_OK) != 0)
    {
        if (_mkdir(sysTempPath) != 0)
        {
            fprintf(stderr, "Can not create skyeye_temp path: %s\n", sysTempPath);
            return;
        }
    }

    time_t now_time = time(0);
    struct tm *ltm = localtime(&now_time);

    sprintf(curr_time, "%d%02d%02d%02d%02d%02d",
            1900 + ltm->tm_year, 1 + ltm->tm_mon, ltm->tm_mday, ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
    sprintf(tempFileName, "%s/skyeye_bugreport_%s.log", sysTempPath, curr_time);

    CallFunction(ExcHndlSetLogFileNameA_func, tempFileName);
}

void ExceptionAfterCallBack()
{
    char cmdLine[256];

    sprintf(cmdLine, "bugreport.exe -f %s -t %s", tempFileName, curr_time);

    STARTUPINFO si;

    ZeroMemory(&si, sizeof (si));
    si.cb = sizeof (si);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_SHOW;
    PROCESS_INFORMATION pi;

    ZeroMemory(&pi, sizeof (pi));
    if (CreateProcess(NULL,             // name of executable module
                      cmdLine,          // command line string
                      NULL,             // process attributes
                      NULL,             // thread attributes
                      FALSE,            // handle inheritance option
                      0,                // creation flags
                      NULL,             // new environment block
                      NULL,             // current directory name
                      &si,              // startup information
                      &pi))             // process information
    {
        CloseHandle(pi.hThread);
        CloseHandle(pi.hProcess);
    }
    exit(1);
}

void backtrace_register(void)
{
#if BX_WITH_BACKTRACE
    SYSTEM_INFO systemInfo;

    GetSystemInfo(&systemInfo);
    OSVERSIONINFOEX os;

    os.dwOSVersionInfoSize = sizeof (OSVERSIONINFOEX);
    if (GetVersionEx((OSVERSIONINFO *) & os))
    {
        //if os is Windows 2000, windows xp, windows server 2003 not register backtrace
        if (os.dwMajorVersion == 5)
            return;
    }

    if (g_hDll == NULL)
    {
        g_hDll = LoadLibrary("exchndl.dll");

        ExcHndlInit_func = GetProcAddress(g_hDll, "ExcHndlInit");
        ExcHndlSetLogFileNameA_func = GetProcAddress(g_hDll, "ExcHndlSetLogFileNameA");
        ExcHndlSetExceptionCallBack_func = GetProcAddress(g_hDll, "ExcHndlSetExceptionCallBack");

        CallFunction(ExcHndlInit_func);
        CallFunction(ExcHndlSetLogFileNameA_func, "bugreport.txt");
        CallFunction(ExcHndlSetExceptionCallBack_func, ExceptionBeforeCallBack, ExceptionAfterCallBack);
    }
#else
    printf("SkyEye Not Register Backtrace!\n");
    return;
#endif
}

void backtrace_unregister(void)
{
}
#endif
